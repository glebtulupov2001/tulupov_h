const API = "https://ajax.test-danit.com/api/swapi/films";
const filmList = document.getElementById("film-list");
function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options });
}

function getFilm() {
  sendRequest(API)
    .then((response) => response.json())
    .then((films) => {
      console.log(films);
      films.forEach(({ episodeId, openingCrawl, name, characters }) => {
        let li = document.createElement("li");
        let id = document.createElement("p");
        let _name = document.createElement("p");
        let crawl = document.createElement("p");

        id.innerHTML = `Episode ${episodeId}`;
        li.append(id);
        _name.innerHTML = `${name}`;
        li.append(_name);
        let charactersList = document.createElement("ul");
        characters.forEach((link) => {
          sendRequest(link)
            .then((response) => response.json())
            .then((character) => {
              
              let chName = document.createElement("li");
              chName.innerHTML = character.name;
              charactersList.append(chName);
              ;
            });
        });
        li.append(charactersList)
        crawl.innerHTML = `${openingCrawl}`;
        li.append(crawl);
        filmList.append(li);

        // filmList.insertAdjacentHTML(
        //   "beforeend",
        //   `<li id="${episodeId} class="film-item"><p>Episode ${episodeId}</p>
        //   <p class="${name}" id="film-name">${name}</p>
        //   <p>${openingCrawl}</p></li>`
        // );
      });
    });
}

getFilm();

// function getCharacters() {
//   sendRequest(API)
//     .then((response) => response.json())
//     .then((films) => {
//       films.forEach((elem) => {
//         const linkArray = elem.characters;
//         // console.log(linkArray);
//         linkArray.forEach((link) => {
//           sendRequest(link)
//             .then((response) => response.json())
//             .then((character) => {
//               let chName = character.name;
//               // console.log(character);
//               character.films.forEach((filmLink) => {
//                 sendRequest(filmLink)
//                   .then((response) => response.json())
//                   .then((film) => {
//                     // console.log(film.name);
//                   });
//               });
//             });
//         });
//       });
//     });
// }

// getCharacters();
