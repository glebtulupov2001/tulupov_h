const APIusers = "https://ajax.test-danit.com/api/json/users";
const APIposts = "https://ajax.test-danit.com/api/json/posts";

let Card = class {
  constructor(postId, name, email, title, body) {
    this.postId = postId;
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
  }
};

function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options });
}

// getUsers();

sendRequest(APIposts)
  .then((response) => response.json())
  .then((posts) => {
    posts.forEach(({ id, title, body, userId }) => {
      let postId = id;
      // console.log(postId);
      let postWrapper = document.createElement("div");
      let nameWrapper = document.createElement("span");
      let userName = document.createElement("h3");
      let userEmail = document.createElement("p");
      let postTitle = document.createElement("h2");
      let postBody = document.createElement("p");
      let deleteBTN = document.createElement("button");

      deleteBTN.className = "delete-btn";
      nameWrapper.className = "name-wrapper";
      postWrapper.className = "post-wrapper";

      sendRequest(APIusers)
        .then((response) => response.json())
        .then((users) => {
          users.forEach(({ id, name, email }) => {
            if (userId === id) {
              let post = new Card(postId, name, email, title, body);
              // console.log(post);
              // console.log(post.postId);
              userName.innerHTML = post.name;
              userEmail.innerHTML = post.email;
              postTitle.innerHTML = post.title;
              postBody.innerHTML = post.body;
              deleteBTN.innerHTML = "Delete post";

              postWrapper.id = post.postId;

              nameWrapper.append(userName);
              nameWrapper.append(userEmail);
              postWrapper.append(nameWrapper);
              postWrapper.append(postTitle);
              postWrapper.append(postBody);
              postWrapper.append(deleteBTN);

            }
          });
        });
      document.body.append(postWrapper);
    });
  });

setTimeout(function () {
  alert("Now you can delete posts");
  let btnArray = Array.from(document.getElementsByClassName("delete-btn"));
  btnArray.forEach((element) => {
    element.addEventListener("click", (event) => {
      event.target.closest("div").style.backgroundColor = "green";
      let postId = event.target.closest("div").id;
      console.log(postId);

      sendRequest(
        `https://ajax.test-danit.com/api/json/posts/${postId}`,
        "DELETE"
      )
        .then((response) => {
          console.log(response);
          if (response.ok) {
            let post = document.getElementById(`${postId}`);
            post.remove();
          } else {
            throw new Error("Server error");
          }
        })
        .catch((error) => {
          console.error(error);
        });
    });
  });
}, 15000);
