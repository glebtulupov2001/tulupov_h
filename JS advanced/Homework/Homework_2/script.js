const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
arrayList(books, root);

function arrayList(arr, root) {
  const list = document.createElement("ol");
  arr.forEach((element) => {
    try {
      if (!element.author || !element.name || !element.price) {
        throw new SyntaxError("Данные некорректны");
      }
      const author = document.createElement("li");
      author.innerHTML = `author: ${element.author}`;
      list.appendChild(author);
      const name = document.createElement("li");
      name.innerHTML = `name: ${element.name}`;
      list.appendChild(name);
      const price = document.createElement("li");
      price.innerHTML = `price: ${element.price}`;
      list.appendChild(price);
      const gap = document.createElement("li");
      list.appendChild(gap);
    } catch (error) {
      alert(`Вибачте, не достатньо даних в елементі з ключем: name: ${element.name} `);
    }
  });
  root.append(list);

}
