let Employee = class {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(name) {
    this._name;
  }
  set age(age) {
    this._age;
  }
  set salary(salary) {
    this._salary;
  }
};

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary * 3);
    this._lang = lang;
  }
  // set salary(salary) {
  //   this._salary * 3;
  // }
  // get salary() {
  //   return this._salary * 3;
  // }
}

const p1 = new Programmer("Sasha", "18", "2000", [
  "English",
  "Ukrainian",
  "Polish",
]);
const p2 = new Programmer("Pasha", "28", "3000", [
  "German",
  "Ukrainian",
  "French",
]);
const p3 = new Programmer("Masha", "38", "5000", [
  "Chinese",
  "Ukrainian",
  "Turkish",
]);

console.log(p1);
console.log(p2);
console.log(p3);
console.log(p3.salary);

