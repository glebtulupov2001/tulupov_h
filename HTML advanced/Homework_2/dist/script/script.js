const screenWidth = window.screen.width;
const itemPlots = document.querySelectorAll(".item-plot");
const changeTitle = document.querySelector(".photo-title");
const lastInstPhoto = document.querySelector(".photo-content");
const firstInstPhoto = document.querySelector(".photo-wrap");
const instPhotos = document.querySelectorAll(".photo-wrap");
const burgerBtn = document.querySelector(".burger-btn");
const burgerOpen = document.querySelector(".burger-open");
const burgerClose = document.querySelector(".burger-close");
const burgerMenu = document.querySelector(".burger-list");

let closeOpen = "open";

if (screenWidth <= 690) {
  instPhotos.forEach((element) => {
    element.style.display = "none";
  });
  instPhotos[0].style.display = "block";
} else if (screenWidth <= 1024 && screenWidth > 640) {
  lastInstPhoto.lastChild.style.display = "none";
}

burgerBtn.addEventListener("click", () => {
  if (closeOpen === "open") {
    closeOpen = "close";
    burgerOpen.style.display = "none";
    burgerClose.style.display = "block";
    burgerMenu.style.visibility = "visible";
  } else {
    closeOpen = "open";
    burgerOpen.style.display = "block";
    burgerClose.style.display = "none";
    burgerMenu.style.visibility = "hidden";
  }
});
