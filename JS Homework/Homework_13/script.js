const images = document.querySelectorAll(".image-to-show");
const btnStart = document.getElementById("start");
const btnStop = document.getElementById("stop");
let i = 0;

function imageChange() {
  images.forEach((element) => {
    element.classList.replace("show", "hidden");
  });
  const id = images[i];
  id.classList.replace("hidden", "show");
  i++;
  if (i > images.length - 1) {
    i = 0;
  }
}

btnStop.addEventListener("click", () => {
  clearInterval(interval);
});
btnStart.addEventListener("click", () => {
  interval = setInterval(imageChange, 3000);
});

let interval = setInterval(imageChange, 3000);
