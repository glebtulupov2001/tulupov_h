const servicesTabs = document.querySelectorAll(".services-item");
const servicesContentList = document.querySelector(".services-content-list");
const servicesContent = Array.from(servicesContentList.children);

servicesTabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    servicesTabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    event.target.classList.add("active");
    servicesContent.forEach((content) => {
      if (
        event.target.className === "services-item active" &&
        event.target.dataset.name === content.dataset.name
      ) {
        content.classList.replace("hidden", "show");
      } else {
        content.classList.replace("show", "hidden");
      }
    });
  });
});

const loadBtn = document.getElementById("load-btn");
loadBtn.addEventListener("click", () => {
  setTimeout(() => {
    amazingWorksContent.forEach((element) => {
      if (element.classList.contains("hidden")) {
        element.classList.replace("hidden", "show");
      }
      loadBtn.style.display = "none";
    });
  }, 1000);
});

const amazingWorkTabs = document.querySelectorAll(".amazing-work-item");
const amazingWorkContentList = document.querySelector(".amazing-work-content");
const amazingWorksContent = Array.from(amazingWorkContentList.children);

amazingWorkTabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    amazingWorkTabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    event.target.classList.add("active");

    amazingWorksContent.forEach((content) => {
      if (
        event.target.className === "amazing-work-item active" &&
        event.target.dataset.sort === content.dataset.sort
      ) {
        content.classList.replace("hidden", "show");
      } else if (
        event.target.className === "amazing-work-item active" &&
        event.target.dataset.all === content.dataset.all
      ) {
        content.classList.replace("hidden", "show");
        loadBtn.style.display = "none";
      } else {
        content.classList.replace("show", "hidden");
      }
    });
  });
});



const carousel = document.querySelectorAll(".carousel-item");
const carouseltList = document.querySelector(".say-about-list");
const carouselContent = Array.from(carouseltList.children);

carousel.forEach((element) => {
  element.addEventListener("click", (event) => {
    carousel.forEach((elem) => {
      elem.classList.remove("choosen");
    });
    event.target.classList.add("choosen");
    carouselContent.forEach((content) => {
      if (
        event.target.className === "carousel-item choosen" &&
        event.target.dataset.worker === content.dataset.worker
      ) {
        content.classList.replace("hide", "see");
      } else {
        content.classList.replace("see", "hide");
      }
    });
  });
});