const btns = document.querySelectorAll(".btn");

document.addEventListener("keyup", (event) => {
  btns.forEach((element) => {
    element.style.backgroundColor = "black";
    if (event.code === element.dataset.code) {
      element.style.backgroundColor = "blue";
    }
  });
});
