const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".hiden");
tabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    tabs.forEach((elem) => {
      elem.classList.remove("active");
    });
    event.target.classList.add("active");
    tabsContent.forEach((content) => {
      if (
        event.target.className === "tabs-title active" &&
        event.target.dataset.name === content.dataset.name
      ) {
        content.className = "show";
      } else {
        content.className = "hiden";
      }
    });
  });
});
