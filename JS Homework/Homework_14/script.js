
const themeBtn = document.querySelector(".theme-btn");
themeBtn.addEventListener("click", () => {
  if (theme === "light") {
    theme = "dark";
    document
      .querySelector('[title="theme"]')
      .setAttribute("href", "./style/dark.css");
  } else if (theme === "dark") {
    theme = "light";
    document
      .querySelector('[title="theme"]')
      .setAttribute("href", "./style/light.css");
  }
  localStorage.setItem("theme", theme);
});




const btns = document.querySelectorAll(".btn");
document.addEventListener("keyup", (event) => {
  if (theme === "light") {
    btns.forEach((element) => {
      element.style.color = "white";
      element.style.backgroundColor = "black";
      if (event.code === element.dataset.code) {
        element.style.backgroundColor = "blue";
        element.style.color = "white";
      }
    });
  } else if (theme === "dark") {
    btns.forEach((element) => {
      element.style.color = "black";
      element.style.backgroundColor = "white";
      if (event.code === element.dataset.code) {
        element.style.backgroundColor = "yellow";
        element.style.color = "black";
      }
    });
  }
});



let curentTheme = localStorage.getItem("theme");
if (curentTheme === null || curentTheme === "light") {
  // Если значение не записано, или оно равно 'light' - применяем светлую тему
  theme = "light"
  document
  .querySelector('[title="theme"]')
  .setAttribute("href", "./style/light.css");
} else if (curentTheme === "dark") {
  // Если значение равно 'dark' - применяем темную
  theme = "dark"
  document
  .querySelector('[title="theme"]')
  .setAttribute("href", "./style/dark.css");
}
