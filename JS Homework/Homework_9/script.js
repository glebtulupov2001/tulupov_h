const array = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  ["1", "2", "3", "sea", "user", 23],
  "Odessa",
  "Lviv",
];

parent = document.body

arrayList(array, parent);

function arrayList(arr, parent) {
  const list = document.createElement("ol");
  arr.forEach((element) => {
    if (Array.isArray(element)) {
      const childList = document.createElement("ul");
      element.forEach((i) => {
        const listChild = document.createElement("li");
        listChild.innerHTML = i;
        childList.appendChild(listChild);
      });
      list.appendChild(childList);
    } else {
      const child = document.createElement("li");
      child.innerHTML = element;
      list.appendChild(child);
    }
  });
  parent.append(list);
}

let sec = 3;
const timer = document.createElement("div");
timer.innerHTML = `Time left: ${sec}`;
parent.append(timer);
function timeLeft() {
  
  sec--
  timer.innerHTML = `Time left: ${sec}`;
  

  

}

function clearPage() {
  parent.innerHTML = " ";
}

setInterval(timeLeft, 1000);
setTimeout(clearPage, 3000);
