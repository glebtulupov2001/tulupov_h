const form = document.querySelector(".password-form");
const inputs = document.getElementsByTagName("input");
const inputsArray = Array.from(inputs);

form.addEventListener("click", (event) => {
  const icon = event.target;
  if (icon.classList.contains("fa-eye")) {
    icon.classList.replace("fa-eye", "fa-eye-slash");
  } else {
    icon.classList.replace("fa-eye-slash", "fa-eye");
  }
  inputsArray.forEach((element) => {
    if (
      icon.classList.contains("fa-eye-slash") &&
      icon.dataset.name === element.dataset.name
    ) {
      element.type = "text";
    }
    if (
      icon.classList.contains("fa-eye") &&
      icon.dataset.name === element.dataset.name
    ) {
      element.type = "password";
    }
  });

  if (
    icon.className === "btn" &&
    inputsArray[0].value === inputsArray[1].value
  ) {
    document.querySelector(".show").className = "hidden";
    alert("You are welcome");
  }
  if (
    icon.className === "btn" &&
    inputsArray[0].value !== inputsArray[1].value
  ) {
    document.querySelector(".hidden").className = "show";
  }
});
