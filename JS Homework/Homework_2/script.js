let name = prompt("Enter your name.");

while (name === "" || name === null) {
  name = prompt("Enter valid name again.");
}

let age = +prompt("Enter your age.");

while (age === null || Number.isNaN(age)) {
  age = +prompt("Enter valid age again.");
}

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  const quetionAnswer = confirm("Are you sure you want to continue?");
  if (quetionAnswer === true) {
    alert(`Welcome, ${name}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${name}`);
}
